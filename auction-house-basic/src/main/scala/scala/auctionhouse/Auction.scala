package scala.auctionhouse

import akka.actor.{Cancellable, ActorRef, Actor}
import akka.event.{Logging, LoggingReceive}
import scala.concurrent.duration.{FiniteDuration, Duration}
import scala.concurrent.ExecutionContext.Implicits.global
import java.util.concurrent.TimeUnit;

/**
 * Reactive Scala Programming 2014/2015. Exercise 1.
 *
 * Created by Magdalena Strzoda on 2014-10-19.
 */

object AuctionHouseMessage {
  case object CreateAuction
  case class BidOnAuction(bidder: ActorRef, amount: Int) {
    require(amount > 0)
  }
  case object IgnoreAuction
  case object DeleteAuction
  case object SellAuction
  case object MakeBid
  case class NotifyBuyer(auctionId : Int)
  case object CloseAuctionHouse
}

class Auction(id: Int, bidTimerLength: Int = 10, deleteTimerLength: Int = 5) extends Actor {
  import AuctionHouseMessage._
  val system = context.system
  val log = Logging(context.system, this)
  private var bidTimer = None:Option[Cancellable]
  private var deleteTimer = None:Option[Cancellable]

  private var highestBid = 0
  private var bidder = None:Option[ActorRef]

  // in iniitial state
  def receive = LoggingReceive {
    case CreateAuction => {
      setUpBidTimer()
      context become auctionCreated()
    }
  }

  // AuctionCreated
  def auctionCreated() : Receive = LoggingReceive {
    case BidOnAuction(newBidder, newBid) => {
      if ( newBid > highestBid ) {
        highestBid = newBid
        bidder = Some(newBidder)
        println(s"Auction $id: first bid - amount $newBid by buyer $newBidder ")
        context become waitForMoreBidders()
      }
      println("Auction " + id + ": bid amount was incorrect.")
    }

    case IgnoreAuction => {
      setUpDeleteTimer()
    }

    case DeleteAuction => {
      println("Auction " + id + ": No bidders. Deleting action.")
      //auctionHouse ! CloseAuctionHouse
      context stop self
    }
  }

  // AuctionActivated
  def waitForMoreBidders(): Receive = LoggingReceive {
    case BidOnAuction(newBidder, newBid) =>
      if ( newBid > highestBid ) {
        highestBid = newBid
        bidder = Some(newBidder)
        println(s"Auction $id: new bid - amount $newBid by buyer $newBidder")
      }
      println(s"Auction $id: new bid - too small bid amount $newBid by buyer $newBidder")

    case IgnoreAuction => {
      println("Auction " + id + ": No more bidders. End of action.")
      setUpDeleteTimer()
    }

    case DeleteAuction => {
      println("Auction " + id + ": Auction won by buyer " + bidder.get)
      println("Auction " + id + ": Deleting action.")
      bidder.get ! NotifyBuyer(id)
      //auctionHouse ! CloseAuctionHouse
      context.stop(self)
    }

  }

  def setUpBidTimer() = {
    log.debug("setUpBidTimer")
    if ( bidTimer.nonEmpty )
      bidTimer.get.cancel()
    bidTimer = Some(system.scheduler.scheduleOnce(FiniteDuration(bidTimerLength, TimeUnit.SECONDS), self, IgnoreAuction))
  }

  def setUpDeleteTimer() = {
    log.debug("setUpDeleteTimer")
//    if ( deleteTimer.nonEmpty )
//      deleteTimer.get.cancel()
    deleteTimer = Some(system.scheduler.scheduleOnce(FiniteDuration(deleteTimerLength, TimeUnit.SECONDS), self, DeleteAuction))
  }

}

