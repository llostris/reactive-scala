package scala.auctionhouse

import akka.actor.{Actor, ActorRef, Props, ActorSystem}
import akka.event.LoggingReceive

import scala.util.Random

/**
 * Reactive Scala Programming 2014/2015. Exercise 1.
 *
 * Created by Magdalena Strzoda on 2014-10-19.
 */


object AuctionHouse {
  import AuctionHouseMessage._

  final val MAX_BID_AMOUNT = 3000
  val rand = new Random(System.currentTimeMillis())
  val system = ActorSystem()

  final val auctionNumber = 3
  final val buyersNumber = 5
  val iterations = 10
  var buyersLeft = buyersNumber

//  def receive = LoggingReceive {
//    case CloseAuctionHouse => {
//      buyersLeft -= 1
//      if ( buyersLeft == 0 )
//        system.shutdown()
//    }
//  }

  def main(args: Array[String]) = {
    run()
  }

  def run() = {
    var i = 0
    val auctions = new Array[ActorRef](auctionNumber)
    val buyers = new Array[ActorRef](buyersNumber)

    while ( i < auctionNumber) {
      auctions(i) = system.actorOf(Props(classOf[Auction], i, 10, 5), "auction_" + i)
      auctions(i) ! CreateAuction
      i += 1
    }
    i = 0
    while ( i < buyersNumber ) {
      buyers(i) = system.actorOf(Props(classOf[Buyer], i, auctions), "buyer_" + i)
      i += 1
    }

    i = 0
    while ( i < iterations) {
      val buyerId = rand.nextInt(buyers.size)
      buyers(buyerId) ! MakeBid
      i+= 1
    }
  }
}

