package scala.auctionhouse

import akka.actor.{Actor, ActorRef}
import akka.event.{Logging, LoggingReceive}

import scala.util.Random

/**
 * Reactive Scala Programming 2014/2015. Exercise 1.
 *
 * Created by Magdalena Strzoda on 2014-10-19.
 */

class Buyer(id : Int, auctions: Array[ActorRef]) extends Actor {
  final val MAX_BID_AMOUNT = 3000

  import scala.auctionhouse.AuctionHouseMessage._
  val log = Logging(context.system, this)
  val rand = new Random(System.currentTimeMillis() + self.path.hashCode)

  override def receive = LoggingReceive {
    case NotifyBuyer(auctionId) => {
      println("Buyer " + id + ": won auction " + auctionId)
    }

    case MakeBid => {
      val auctionId = rand.nextInt(auctions.size)
      val bidAmount = rand.nextInt(MAX_BID_AMOUNT)
      auctions(auctionId) ! BidOnAuction(self, bidAmount)
    }
  }
}
