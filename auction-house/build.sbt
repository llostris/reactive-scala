name := "auction-house"

version := "1.0"

fork := true

libraryDependencies ++= {
  val akkaVersion = "2.3.6"
  val sprayVersion = "1.3.2"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-persistence-experimental" % akkaVersion,
    "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
    "com.typesafe.akka" %% "akka-persistence-tck-experimental" % akkaVersion,
    "org.scalatest" %% "scalatest" % "1.9.2-SNAP2",
    "com.okumin" %% "akka-persistence-sql-async" % "0.1",
    "com.github.mauricio" %% "postgresql-async" % "0.2.15",
    "io.spray" %% "spray-can" % sprayVersion,
    "io.spray" %% "spray-routing" % sprayVersion,
    "io.spray" %% "spray-client" % sprayVersion,
    "io.spray" %% "spray-httpx" % sprayVersion,
    "io.spray" %% "spray-json" % "1.3.1",
    "io.spray" %% "spray-testkit" % sprayVersion % "test",
    "org.json4s" %% "json4s-native" % "3.2.11",
    "org.json4s" %% "json4s-jackson" % "3.2.11"
  )
}

addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "1.6.0")

//Revolver.settings