package pl.edu.agh.scala.publisher

import akka.event.LoggingReceive
import com.typesafe.config.{ConfigFactory, Config}
import spray.can.Http
import spray.httpx.unmarshalling._
import spray.http.StatusCodes._
import spray.http._
import spray.routing._
import spray.util._

/**
 * Reactive Scala Programming 2014/2015. Exercise 6.
 *
 * Created by Magdalena Strzoda on 2014-11-26.
 */


class AuctionPublisher extends HttpServiceActor {
  import pl.edu.agh.scala.auctionhouse.NotifierMessage._

  lazy val config: Config = ConfigFactory.load("application-server.conf")

  override def receive = runRoute(myRoute)

  val myRoute =
    path("") {
      post {
        entity(as[Notify]) { notify =>
          publishAuction(notify)
          complete{
            StatusCodes.OK
          }
        }
      }
    }

  def publishAuction(notify : Notify): Unit = {
    println(s"Auction ${notify.auctionTitle} : new bid ${notify.highestBid} by ${notify.highestBidder}")
  }

//    override def receive: Receive = LoggingReceive {
//
//      case _: Http.Connected =>
//        sender() ! Http.Register(self)
//
//      case HttpRequest(HttpMethods.POST, _, _, entity : HttpEntity, _) => {
//        publishAuction(entity.as[Notify].get)
//        sender ! HttpResponse(StatusCodes.OK)
//      }
//
//    }

  implicit def myExceptionHandler(implicit log: LoggingContext) =
    ExceptionHandler {
      case e: Exception =>
        requestUri { uri =>
          log.warning("Request to {} could not be handled normally", uri)
          complete(InternalServerError, "Error occurred!")
        }
    }
}
