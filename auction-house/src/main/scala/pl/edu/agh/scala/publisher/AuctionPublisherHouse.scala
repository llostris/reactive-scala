package pl.edu.agh.scala.publisher

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import akka.util.Timeout
import akka.pattern.ask
import spray.can.Http
import scala.concurrent.duration._

import scala.util.Random

/**
 * Reactive Scala Programming 2014/2015. Exercise 1.
 *
 * Created by Magdalena Strzoda on 2014-10-13.
 */

object AuctionPublisherHouse {

  val rand = new Random(System.currentTimeMillis())
  val system = ActorSystem()

  def main(args: Array[String]) : Unit = {
    run()
  }

  def run() = {
    // we need an ActorSystem to host our application in
    implicit val system = ActorSystem("spray-auction-publisher")

    // create and start our service actor
    val service = system.actorOf(Props[AuctionPublisher], "auction-publisher")

    implicit val timeout = Timeout(5.seconds)
    // start a new HTTP server on port 8080 with our service actor as the handler
    IO(Http) ? Http.Bind(service, interface = "localhost", port = 8080)
  }

}

