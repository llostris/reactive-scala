package pl.edu.agh.scala.auctionhouse

import akka.actor.{Actor, ActorRef}
import akka.event.{Logging, LoggingReceive}
import pl.edu.agh.scala.auctionhouse.performance.StatisticMessage
import StatisticMessage.Elapsed
import pl.edu.agh.scala.auctionhouse.AuctionHouseMessage.{MakeBidOnInterestingAuction, BidOnAuction, MakeRandomBid}
import pl.edu.agh.scala.auctionhouse.AuctionNotification.{RegisterForNotifications, NotifyBidder, NotifyWinner}
import pl.edu.agh.scala.auctionhouse.AuctionSearchMessage.{FindAuction, FindAuctionReply}

import scala.util.Random

/**
 * Reactive Scala Programming 2014/2015. Exercise 1.
 *
 * Created by Magdalena Strzoda on 2014-10-13.
 */


class Buyer(id : Int, moneyInWallet : Int) extends Actor {
  final val MAX_BID_AMOUNT = 3000

  private val system = context.system
  private val log = Logging(context.system, this)
  private val rand = new Random(System.currentTimeMillis() + self.path.hashCode)

  var startTimes: List[Long] = List()

  override def receive = LoggingReceive {

    case NotifyWinner(auctionId) => {
      println(s"Buyer $id: won auction $auctionId")
    }

    case NotifyBidder(currentBid, auctionId) => {
      log.debug(s"Buyer $id: received NotifyBidder")
      if ( currentBid + 10 < moneyInWallet )
        sender ! BidOnAuction(self, currentBid + 10)
      else {
        log.info(s"Buyer $id doesn't have enough cash to bid on auction $auctionId")
      }
    }

    case MakeBidOnInterestingAuction(keyword) => {
      val auctionSearch = system.actorSelection("/user/MasterSearch")
      startTimes :+= System.nanoTime()
      auctionSearch ! FindAuction(keyword)
    }


    case FindAuctionReply(auctions : List[ActorRef]) => {
      sendPerformanceTime()
      for (auction <- auctions) {
        val bidAmount = rand.nextInt(moneyInWallet)
        auction ! BidOnAuction(self, bidAmount)
        auction ! RegisterForNotifications(self) // todo: not always?
        log.debug(s"Buyer $id: made bid on auction $auction")
      }
      context become receive
    }

  }

  def waitingForAuctionSearch(keyword : String) = LoggingReceive {

    case FindAuctionReply(auctions : List[ActorRef]) => {
      for (auction <- auctions) {
        val bidAmount = rand.nextInt(moneyInWallet)
        auction ! BidOnAuction(self, bidAmount)
        auction ! RegisterForNotifications(self) // todo: not always?
        log.debug(s"Buyer $id: made bid on auction $auction")
      }
      context become receive
    }

  }

  def sendPerformanceTime(): Unit = {
    if ( startTimes.size > 0 ) {
      context.actorSelection("akka://default/user/Stats") ! Elapsed(System.nanoTime() - startTimes.reverse.head)
    }
  }

}
