package pl.edu.agh.scala.auctionhouse.performance

import akka.actor.Actor
import akka.event.{Logging, LoggingReceive}
import pl.edu.agh.scala.auctionhouse.performance.StatisticMessage.Elapsed

object StatisticMessage {
  case class Elapsed(nanoSeconds : Long)
}

class StatisticsCollector() extends Actor {
  val log = Logging(context.system, self)

  var times: List[Long] = List()

  override def receive: Actor.Receive = LoggingReceive {
    case Elapsed(nanos: Long) => {
      times :+= nanos
      println("MEAN\t75.0%\t90.0%")
      println(logMean() + "\t" + logRange(0.75) + "\t" + logRange(0.9))
    }
  }

  def sum(xs: List[Long]): Long = {
    xs.foldLeft(0L)(_ + _)
//    xs match {
//      case x :: tail => x + sum(tail)
//      case Nil => 0
//    }
  }

  def logMean(): Long = {
    sum(times) / times.size
  }

  def logRange(rangeSpan: Double): Long = {
    val sorted: List[Long] = times.sortWith(_ > _)
    val trimmed: List[Long] = sorted.drop((sorted.size * (1 - rangeSpan)).toInt)
    trimmed.head
  }
}
