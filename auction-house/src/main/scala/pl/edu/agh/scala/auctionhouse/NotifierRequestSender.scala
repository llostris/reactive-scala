package pl.edu.agh.scala.auctionhouse

import akka.actor.{ActorRef, Actor}
import akka.actor.Actor.Receive
import akka.event.LoggingReceive
import spray.http.ContentTypes._
import spray.httpx.marshalling.Marshaller
import scala.concurrent.duration._
import pl.edu.agh.scala.auctionhouse.NotifierMessage.{Sent, Notify}
import spray.client.pipelining._
import spray.http.{StatusCodes, HttpEntity, HttpResponse, HttpRequest}
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

/**
 * Created by Magda on 2014-11-29.
 */
class NotifierRequestSender extends Actor {
  var state = 0


  override def receive: Receive = LoggingReceive {

    case msg@Notify(auctionTitle : String, bidder : String, highestBid : Int) => {
      val pipeline: HttpRequest => Future[HttpResponse] = sendReceive

      val response: Future[HttpResponse] = pipeline(Post("http://localhost:8080/", Notify(auctionTitle, bidder, highestBid)))

      response onComplete {
        case Success(r) =>  {
          if ( r.status.intValue == StatusCodes.OK.intValue )
            println(s"Msg successfuly sent: $msg")
        }
        case Failure(e) => {
          println("An error has occurred: " + e.getMessage)
        }
      }
      Await.result(response, 2 seconds)
      sender ! Sent
    }
  }

}
