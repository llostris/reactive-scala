package pl.edu.agh.scala.auctionhouse

import akka.actor.{ActorRef, ActorSystem, Props}

import scala.util.Random
import collection.JavaConversions._

/**
 * Reactive Scala Programming 2014/2015. Exercise 1.
 *
 * Created by Magdalena Strzoda on 2014-10-13.
 */

object AuctionHouse {
  import AuctionHouseMessage._

  val data = AuctionHouseTestData

  final val MAX_BID_AMOUNT = 3000
  val rand = new Random(System.currentTimeMillis())
  val system = ActorSystem()

  def main(args: Array[String]) = {
    run()
  }

  def run() = {
    system.actorOf(Props(classOf[MasterSearch], 3), "MasterSearch")
    system.actorOf(Props[Notifier], "Notifier")

    val sellers = new Array[ActorRef](data.sellersNumber)
    val buyers = new Array[ActorRef](data.buyersNumber)


    var iter = 0
    while ( iter < data.iterations ) {

      // create actors
      var i = 0
      while (i < data.sellersNumber) {
        sellers(i) = system.actorOf(Props(classOf[Seller], iter * 100 + i, data.auctionNames(i)), "seller_" + iter + "_" + i)
        var j = 0
        while (j < data.auctionNames(i).length) {
          sellers(i) ! PutOnAuction(j)
          j += 1
        }
        i += 1
      }

      i = 0
      while (i < data.buyersNumber) {
        buyers(i) = system.actorOf(Props(classOf[Buyer], iter * 100 + i, data.buyerMaxBidAmount(i)), "buyer_" + iter + "_" + i)
        i += 1
      }

      Thread.sleep(2000)  // let all the registrations take place

      var queries = 0
      while ( queries < data.queriesPerIteration ) {
        i = 0
        while (i < data.buyersNumber) {
          for (keyword <- data.buyerKeywords(i)) {
            buyers(i) ! MakeBidOnInterestingAuction(keyword)
            Thread.sleep(500)
          }
          i += 1
        }
        queries += 1
      }

      iter += 1
    }

  }

}

