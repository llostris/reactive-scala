package pl.edu.agh.scala.auctionhouse

import java.util.concurrent.TimeUnit

import akka.actor.{Cancellable, ActorRef}
import akka.event.{Logging, LoggingReceive}
import akka.persistence._
import pl.edu.agh.scala.auctionhouse.NotifierMessage.{Initialize, Notify}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.FiniteDuration

/**
 * Reactive Scala Programming 2014/2015. Exercise 4.
 *
 * Created by Magdalena Strzoda on 2014-10-13.
 */

sealed trait State
case object AuctionCreated extends State
case object AuctionIgnored extends State
case object AuctionActivated extends State
case object AuctionSold extends State
case object AuctionRemoved extends State

case class AuctionStateChangeEvent(state : State, data : AuctionData, passedTime : Long)

sealed trait Data
case object Uninitialized extends Data
case class AuctionData(bidder: ActorRef, highestBid: Int) extends Data


class Auction(id : Int, seller : ActorRef, title : String) extends PersistentActor  {
  import AuctionHouseMessage._
  import AuctionNotification._

  override def persistenceId = "persistent-auction"
  private val system = context.system
  private val log = Logging(context.system, this)
  private final val BID_TIMER = "BidTimer"
  private final val DELETE_TIMER = "DeleteTimer"
  var bidTimer = None:Option[Cancellable]
  var deleteTimer = None:Option[Cancellable]

  private var toNotify = List[ActorRef]()
  private val deleteTimerLength = 10
  private var bidTimerLength = 30000L
  private var bidTimerElapsed = 0L
  private var relisted = false
  private var state = AuctionData(null, 0)

  // STATE MACHINE

  // initial state
  override def receive = LoggingReceive {

    case CreateAuction => {
      log.info(s"AuctionCreated, Uninitialized, title: $title")
      setUpBidTimer()
      persist(AuctionStateChangeEvent(AuctionCreated, AuctionData(null, 0), System.currentTimeMillis() - bidTimerElapsed)) {
        event =>
          updateState(event)
      }
    }

  }

  // when auction is created
  def auctionCreated(data : AuctionData) : Receive = LoggingReceive {

    case BidOnAuction(bidder, bid) => {
      cancelTimer(DELETE_TIMER)
      println(s"Auction $id : first bid $bid by $bidder")
      notifyAuctionPublisher(AuctionData(bidder, bid))
      persist(AuctionStateChangeEvent(AuctionActivated, AuctionData(bidder, bid), System.currentTimeMillis() - bidTimerElapsed)) {
        event =>
          updateState(event)
      }
    }

    case RegisterForNotifications(buyer) => {
      log.debug(s"Auction $id: $buyer registered for notifiactions")
      toNotify = toNotify :+ buyer
    }

    case UnregisterFromNotifications(buyer) => {
      toNotify = toNotify diff List(buyer)
    }

    case IgnoreAuction => {
      log.debug("AuctionCreated, IgnoreAuction")
      setUpBidTimer()
      setUpDeleteTimer()
      if ( !relisted )
        seller ! AuctionRelistingPossible
      persist(AuctionStateChangeEvent(AuctionIgnored, data, System.currentTimeMillis() - bidTimerElapsed)) {
        event =>
          updateState(event)
      }
    }
  }

  // when auction is activated (bid is on)
  def auctionActivated(data : AuctionData) : Receive = LoggingReceive {

    case BidOnAuction(bidder, bid) => {
      if (bid > data.highestBid) {
        println(s"Auction $id : New bid $bid by $bidder")
        //setUpBidTimer()
        notifyAuctionPublisher(AuctionData(bidder, bid))
        handleNotifications(bid, bidder)
        persist(AuctionStateChangeEvent(AuctionActivated, AuctionData(bidder, bid), System.currentTimeMillis() - bidTimerElapsed)) {
          event =>
            updateState(event)
        }
      } else {
        println("Auction " + id + ": new bid smaller than current highest bid")
      }
    }

    case RegisterForNotifications(buyer) => {
      log.debug(s"Auction $id: $buyer registered for notifiactions")
      toNotify = toNotify :+ buyer
    }

    case UnregisterFromNotifications(buyer) => {
      toNotify = toNotify diff List(buyer)
    }

    case IgnoreAuction => {
      log.info("Auction " + id + ": no more bids.")
      println("Auction " + id + ": no more bids.")
      setUpDeleteTimer()
      persist(AuctionStateChangeEvent(AuctionSold, data, System.currentTimeMillis() - bidTimerElapsed)) {
        event =>
          updateState(event)
      }
    }
  }

  def auctionIgnored(data : AuctionData) : Receive = LoggingReceive {
    
    case DeleteAuction => {
      log.debug("Auction Ignored, DeleteAuction")
      log.info("Auction " + id + ": no bidders. Removing auction")
      persist(AuctionStateChangeEvent(AuctionRemoved, data, System.currentTimeMillis() - bidTimerElapsed)) {
        event =>
          updateState(event)
//          context stop self
      }
    }

    case IgnoreAuction => {}

    case RelistAuction => {
      relisted = true
      persist(AuctionStateChangeEvent(AuctionCreated, data, System.currentTimeMillis() - bidTimerElapsed)) {
        event =>
          updateState(event)
      }
    }
      
  }

  def auctionSold(data : AuctionData) : Receive = LoggingReceive {

    case DeleteAuction => {
      println(s"Auction $id sold for ${data.highestBid} to ${data.bidder}")
      data.bidder ! NotifyWinner(id)
      persist(AuctionStateChangeEvent(AuctionRemoved, data, System.currentTimeMillis() - bidTimerElapsed)) {
        event =>
          updateState(event)
//          context stop self
      }
    }
      
  }

//  whenUnhandled {
//    case Event(e, s) => {
//      log.warning("State {}: Received invalid message: {} {}", stateName, e, s)
//      stay()
//    }
//  }

  def auctionRemoved(data : AuctionData) : Receive = LoggingReceive {
    case _ => {
      println(s"Auction $id was deleted")
    }
  }

  def setUpBidTimer() = {
    log.debug("setUpBidTimer: " + bidTimerLength)
    //cancelTimer(BID_TIMER)
    bidTimer = Some(system.scheduler.scheduleOnce(FiniteDuration(bidTimerLength, TimeUnit.MILLISECONDS), self, IgnoreAuction))
    bidTimerElapsed = System.currentTimeMillis()
  }

  def setUpDeleteTimer() = {
    log.debug("setUpDeleteTimer")
    cancelTimer(DELETE_TIMER)
    deleteTimer = Some(system.scheduler.scheduleOnce(FiniteDuration(deleteTimerLength, TimeUnit.SECONDS), self, DeleteAuction))
  }

  def cancelTimer(timerName : String ): Unit = {
    if ( timerName.equals(BID_TIMER) )
      if ( bidTimer.nonEmpty )
        bidTimer.get.cancel()
    else
      if ( deleteTimer.nonEmpty )
        deleteTimer.get.cancel()
  }

  def handleNotifications(highestBid : Int, bidder : ActorRef) = {
     for ( bidderToNotify <- toNotify ) {
       if ( bidderToNotify != bidder )
        bidderToNotify ! NotifyBidder(highestBid, self)
     }
  }

  def notifyAuctionPublisher(data : AuctionData) = {
    log.debug(s"Auction $id: sending Notification to Notifier")
    val notifier  = system.actorSelection("/user/Notifier")
    notifier ! Notify(title, data.bidder.toString(), data.highestBid)
  }

  def updateState(event: AuctionStateChangeEvent) = {
    context.become(
      event.state match {
        case AuctionCreated => auctionCreated(event.data)
        case AuctionActivated => auctionActivated(event.data)
        case AuctionIgnored => auctionIgnored(event.data)
        case AuctionSold => auctionSold(event.data)
        case AuctionRemoved => auctionRemoved(event.data)
      }
    )
  }

  override def receiveRecover: Receive = {

    case AuctionStateChangeEvent(oldState : State, data : AuctionData, timeElapsed : Long) => {
      bidTimerLength = math.max(bidTimerLength - timeElapsed, 0)
      updateState(AuctionStateChangeEvent(oldState, data, timeElapsed))
    }

    case RecoveryCompleted => {
      setUpBidTimer()
    }

  }

  override def receiveCommand: Receive = {
    case evt : AuctionStateChangeEvent => updateState(evt)
  }
}
