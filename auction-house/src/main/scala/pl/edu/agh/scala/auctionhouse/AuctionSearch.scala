package pl.edu.agh.scala.auctionhouse

import akka.actor.{ActorRef, Actor}
import akka.event.{Logging, LoggingReceive}

/**
 * Reactive Scala Programming 2014/2015. Exercise 2.
 *
 * Created by Magdalena Strzoda on 2014-10-20.
 */

object AuctionSearchMessage {
  case class FindAuction(keyword : String)  // todo: add require = not null
  case class Register(auction : ActorRef, title : String)
  case class FindAuctionReply(auctions : List[ActorRef])
}

class AuctionSearch extends Actor {
  import AuctionSearchMessage._

  val log = Logging(context.system, this)
  var auctions = Map[String, ActorRef]()

  override def receive = LoggingReceive {

    case FindAuction(keyword) => {
      var auctionsWithKeyword = List[ActorRef]()
      for ( (title, auctionRef) <- auctions ) {
        if ( title contains keyword ) {
          auctionsWithKeyword = auctionsWithKeyword :+ auctionRef
          log.debug(s"AUCTION SEARCH: found '$keyword' in $auctionRef")
        }
      }
      sender() ! FindAuctionReply(auctionsWithKeyword)
    }

    case Register(auction, title) => {
      auctions = auctions + (title -> auction)
      log.debug(s"AuctionSearch: $auction with title: $title registered")
    }
  }

}
