package pl.edu.agh.scala.auctionhouse.performance

import akka.actor._
import pl.edu.agh.scala.auctionhouse.AuctionHouseMessage.MakeBidOnInterestingAuction
import pl.edu.agh.scala.auctionhouse.{AuctionHouseTestData, Buyer, MasterSearch, Seller}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

/**
 * Reactive Scala Programming 2014/2015. Exercise 3.
 *
 * Created by Magdalena Strzoda on 2014-10-15.
 */

object AuctionHousePerformance {

  val data = AuctionHouseTestData
  val system = ActorSystem()

  val AUCTIONS_COUNT = 10000
  val BUYERS_COUNT = 50
  val BUYERS_ACTIVE = 1
  val INTERVAL = 75
  val SEARCH_ACTORS_COUNT = 10

  def main(args: Array[String]) : Unit = {
    run()
  }

  def run() = {
    system.actorOf(Props[StatisticsCollector], "Stats")
    system.actorOf(Props(classOf[MasterSearch], SEARCH_ACTORS_COUNT), "MasterSearch")
    system.actorOf(Props(new Seller(0, Array.fill(AUCTIONS_COUNT)("auction1"))), "seller")

    var buyers = List[ActorRef]()
    for ( i <- 1 to BUYERS_COUNT) {
      buyers :+= system.actorOf(Props(new Buyer(0, 600)), "buyer" + i)
    }

    var scheduled = List[Cancellable]()
    for ( i <- 1 to BUYERS_ACTIVE ) {
      scheduled :+= system.scheduler.schedule(100 milliseconds,
        INTERVAL milliseconds,
        buyers(i),
        MakeBidOnInterestingAuction("keyword"))
    }

    Thread.sleep(15000)

    for ( task <- scheduled ) {
      task.cancel()
    }
    system.shutdown()
  }
}
