package pl.edu.agh.scala.auctionhouse

import java.util.concurrent.TimeoutException

import akka.actor.Actor.Receive
import akka.actor.SupervisorStrategy.Restart
import akka.actor.{OneForOneStrategy, Props, ActorRef, Actor}
import akka.event.LoggingReceive
import akka.pattern.ask
import akka.util.Timeout
import spray.http.ContentTypes.{`application/json`, `text/plain`}
import spray.http._
import spray.httpx.unmarshalling._
import spray.httpx.marshalling._
import scala.concurrent.TimeoutException
import scala.concurrent.{TimeoutException, Future, Await}
import scala.concurrent.duration._

/**
 * Reactive Scala Programming 2014/2015. Exercise 6.
 *
 * Created by Magdalena Strzoda on 2014-11-26.
 */

object NotifierMessage {
  case class Notify(auctionTitle : String, highestBidder : String, highestBid : Int )

  case object Initialize
  case object Send
  case object Sent
  case object NotifyError

  implicit val notifyMarshaller : Marshaller[Notify] = Marshaller.of[Notify](`application/json`) {
//    (value, ct, ctx) => ctx.marshalTo(HttpEntity(ct, s"{ auctionTitle: ${value.auctionTitle}, highestBidder: ${value.highestBidder}, ${value.highestBid} }"))
    (value, ct, ctx) => ctx.marshalTo(HttpEntity(ct, s"${value.auctionTitle}, ${value.highestBidder}, ${value.highestBid}"))
  }

  implicit val notifyUnmarshaller = Unmarshaller[Notify](MediaTypes.`application/json`) {
    case HttpEntity.NonEmpty(contentType, data) =>
      val Array(title, bidder, highestBid) = data.asString.split(", ")
      Notify(title, bidder, highestBid.toInt)
  }

}

class Notifier extends Actor {
  import NotifierMessage._
  val system = context.system
  implicit val timeout = Timeout(5 seconds)
  var notifications : List[Notify] = List[Notify]()
  var notificator : ActorRef = initializeNotifierRequestSender()

  override val supervisorStrategy = OneForOneStrategy(maxNrOfRetries = 5, withinTimeRange = 1 minute) {
    case _: TimeoutException =>
      println("Error sending notification. Restarting.")
      Restart
  }

  override def receive: Receive = afterNotifierInitialized
  
  def afterNotifierInitialized = LoggingReceive {
    case msg : Notify =>
      notifications = notifications :+ msg
      self ! Send

    case Sent =>
      notifications = notifications.patch(0, Nil, 1)

    case Send => {
      val future = notificator ? notifications(0)
      try {
        Await.result(future, timeout.duration) match {
          case Sent =>
            notifications = notifications.patch(0, Nil, 1)
          case _ =>
            println("Notification error. Retrying...")
            self ! Send
        }
      } catch {
        case _ : TimeoutException =>
          println("Retrying...")
          self ! Send
      }
    }
  }


  def initializeNotifierRequestSender() : ActorRef = {
    context.actorOf(Props(new NotifierRequestSender()))
  }
}
