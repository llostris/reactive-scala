package pl.edu.agh.scala.auctionhouse

import akka.actor.{ActorRef, Actor, Props}
import akka.event.{Logging, LoggingReceive}
import pl.edu.agh.scala.auctionhouse.AuctionNotification.AuctionRelistingPossible
import pl.edu.agh.scala.auctionhouse.AuctionSearchMessage.Register

/**
 * Reactive Scala Programming 2014/2015. Exercise 2.
 *
 * Created by Magdalena Strzoda on 2014-10-20.
 */

class Seller(id : Int, auctionNames: Array[String]) extends Actor {
  import AuctionHouseMessage._

  val system = context.system
  val log = Logging(context.system, this)

  override def receive = LoggingReceive {

    case PutOnAuction(auctionId : Int) => {
      if ( auctionId < 0 && auctionId >= auctionNames.length ) {
        log.error(s"SELLER $id: INVALID AUCTION ID: $auctionId")
      }
      val realAuctionId = createAuctionId(auctionId)
      val auction = system.actorOf(Props(classOf[Auction], realAuctionId, self, auctionNames(auctionId)), "auction_s" + id + "_" + auctionId)
      auction ! CreateAuction
      registerInAuctionSearch(auction, auctionNames(auctionId))
      log.debug(s"Seller $id created auction number $realAuctionId with title: ${auctionNames(auctionId)}")
    }


    case AuctionRelistingPossible => {
      sender ! RelistAuction
    }
  }

  def createAuctionId(auctionId: Int) : Int = {
    id * 100 + auctionId
  }

  def registerInAuctionSearch(auction : ActorRef, title : String) = {
    val auctionSearch = system.actorSelection("/user/MasterSearch")
    auctionSearch ! Register(auction, title)
    log.debug(s"Auction ${auction} sent registration message to AuctionSearch")
  }

}
