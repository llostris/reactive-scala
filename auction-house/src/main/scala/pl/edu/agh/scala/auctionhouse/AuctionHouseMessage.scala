package pl.edu.agh.scala.auctionhouse

import akka.actor.ActorRef

/**
 * Reactive Scala Programming 2014/2015.
 *
 * Created by Magdalena Strzoda on 2014-10-29.
 */

object AuctionHouseMessage {
  case object CreateAuction
  case class BidOnAuction(bidder: ActorRef, amount: Int) {
    require(amount > 0)
  }
  case object IgnoreAuction
  case object DeleteAuction
  case object SellAuction
  case object MakeRandomBid
  case class MakeBidOnInterestingAuction(keyword : String)
  case class PutOnAuction(auctionId : Int)
  case class RelistAuction
}

object AuctionNotification {
  case class RegisterForNotifications(buyerId: ActorRef)
  case class UnregisterFromNotifications(buyerId: ActorRef)
  case class NotifyWinner(auctionId: Int)
  case class NotifyBidder(highestBid: Int, auctionId: ActorRef)
  case object AuctionRelistingPossible
}