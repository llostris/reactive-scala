package pl.edu.agh.scala.auctionhouse

import java.util.concurrent.TimeUnit

import akka.actor.{Terminated, ActorRef, Props, Actor}
import akka.event.{Logging, LoggingReceive}
import akka.routing._
import akka.util.Timeout
import pl.edu.agh.scala.auctionhouse.AuctionSearchMessage.{FindAuctionReply, FindAuction, Register}
import pl.edu.agh.scala.auctionhouse.SearchRoutingLogic.{ReplicationRoutingLogicWithDynamicPool, ReplicationRoutingLogic, PartitionRoutingLogic}


/**
 * Reactive Scala Programming 2014/2015. Exercise 3.
 *
 * Created by Magdalena Strzoda on 2014-11-02.
 */

object SearchRoutingLogic {
  case object PartitionRoutingLogic
  case object ReplicationRoutingLogic
  case object ReplicationRoutingLogicWithDynamicPool
}

class MasterSearch(children : Int) extends Actor {

  final val possibleRoutingLogics = Array(PartitionRoutingLogic, ReplicationRoutingLogic, ReplicationRoutingLogicWithDynamicPool)

  private val logic = possibleRoutingLogics(1)
  private val log = Logging(context.system, this)
  private val resizer = DefaultResizer(lowerBound = 1, upperBound = 25)
  private var searchActors = List[ActorRef]()

  implicit val timeout = Timeout(1, TimeUnit.SECONDS)

  var router = {
    val routees = Vector.fill(children) {
      val r = context.actorOf(Props[AuctionSearch])
      context watch r
      searchActors = searchActors :+ r
      ActorRefRoutee(r)
    }

    if ( logic == ReplicationRoutingLogicWithDynamicPool ) {
      RoundRobinPool(5, Some(resizer)).createRouter(context.system).withRoutees(routees)
    } else {
      Router(RoundRobinRoutingLogic(), routees)
    }
  }


  override def receive = {
    if ( logic == PartitionRoutingLogic )
      partitionReceive
    else if ( logic == ReplicationRoutingLogic )
      replicationReceive
    else if ( logic == ReplicationRoutingLogicWithDynamicPool ) {
      replicationWithDynamicPoolReceive
    } else {
      case AnyRef => {
        log.error("Invalid routing logic has been set!")
      }
    }
  }

  def partitionReceive = LoggingReceive {

    case msg : Register => {
      router.route(msg, sender())
    }

    case msg : FindAuction => {
      router.route(Broadcast(msg), sender())
    }

  }

  def replicationReceive = LoggingReceive {

    case msg : Register => {
      router.route(Broadcast(msg), sender())
    }

    case msg : FindAuction => {
      router.route(msg, sender())
    }

  }

  def replicationWithDynamicPoolReceive = LoggingReceive {

    case msg : Register => {
      router.route(Broadcast(msg), sender())
    }

    case msg : FindAuction => {
      router.route(msg, sender())
    }

    case Terminated(justTerminated) => {
      router = router.removeRoutee(justTerminated)
      val newRoutee = context.actorOf(Props[AuctionSearch])
      context watch newRoutee
      router = router.addRoutee(newRoutee)
    }

  }


}
