package pl.edu.agh.scala.auctionhouse

import java.util.concurrent.ConcurrentHashMap

import akka.actor.{ActorRef, ActorSystem, Props}

import scala.collection.concurrent.Map
import scala.collection.mutable
import collection.JavaConversions._
import scala.util.Random

/**
 * Reactive Scala Programming 2014/2015. Exercise 1.
 *
 * Created by Magdalena Strzoda on 2014-10-13.
 */

object AuctionHouseTestData {

  final val MAX_BID_AMOUNT = 3000
  val rand = new Random(System.currentTimeMillis())
  val system = ActorSystem()

  val iterations = 1
  val queriesPerIteration = 1

  val searchActorsNumber = 3
  val auctionNumber = 3
  val buyersNumber = 5
  val sellersNumber = 3
  val auctionNames = Array(
    Array("Audi A6 diesel manual", "Tolkien's Lord of the Rings manuscript", "How to train your dragon movie"),
    Array("Nikon D200 camera", "Canon Powershot camera", "House M.D. season 1 dvd"),
    Array("Samsung phone", "Mass Effect 1-3 game bundle", "Human Deus Ex game", "New Years Eve ticket"),
    Array("Anathema concert ticket", "new Archive album", "The Well of Lost Plots - book")
  )
  val buyerKeywords = Array(
    Array("camera", "diesel", "ticket"),
    Array("game", "ticket", "camera"),
    Array("concert", "album", "Tolkien"),
    Array("game", "ticket", "dvd"),
    Array("manuscript", "album", "movie")
  )
  val buyerMaxBidAmount = Array(
    300, 1000, 2000, 500, 1500
  )

}

object AuctionHousePerformanceData {
  var map = new ConcurrentHashMap[Long, Int]()
}

