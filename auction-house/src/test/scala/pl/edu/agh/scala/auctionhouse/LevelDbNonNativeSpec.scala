package pl.edu.agh.scala.auctionhouse

import java.io.File

import akka.persistence.journal.{JournalPerfSpec, JournalSpec}
import com.typesafe.config.{Config, ConfigFactory}
import org.iq80.leveldb.util.FileUtils
import org.scalatest.Ignore

import scala.util.Random

/**
 * Reactive Scala Programming 2014/2015. Exercise 4.
 *
 * Created by Magdalena Strzoda on 2014-11-24.
 */

@Ignore
class LevelDbNonNativeSpec extends JournalSpec with JournalPerfSpec {
  override lazy val config: Config = ConfigFactory.parseString(
    """
       |akka.persistence.journal.plugin = "akka.persistence.journal.leveldb"
       |akka.persistence.journal.leveldb.native = off
    """.stripMargin)

  val data = AuctionHouseTestData
  val MAX_BID_AMOUNT = 3000
  val rand = new Random(System.currentTimeMillis())

  val storageLocations = List(
    new File(system.settings.config.getString("akka.persistence.journal.leveldb.dir"))
  )

  override def beforeAll() {
    super.beforeAll()
    storageLocations foreach FileUtils.deleteRecursively
  }

  override def afterAll() {
    storageLocations foreach FileUtils.deleteRecursively
    super.afterAll()
  }

}
