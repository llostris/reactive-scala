package pl.edu.agh.scala.auctionhouse

import akka.persistence.helper.PostgreSQLInitializer
import akka.persistence.journal.{JournalPerfSpec, JournalSpec}

/**
 * Reactive Scala Programming 2014/2015. Exercise 4.
 *
 * Created by Magdalena Strzoda on 2014-11-24.
 */


class PostgreSQLJournalSpec extends JournalSpec with JournalPerfSpec with PostgreSQLInitializer{
}
