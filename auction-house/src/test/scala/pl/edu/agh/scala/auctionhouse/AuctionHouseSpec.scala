package pl.edu.agh.scala.auctionhouse

import java.util.concurrent.TimeUnit

import akka.actor.{PoisonPill, ActorRef, Props, ActorSystem}
import akka.testkit.{TestActorRef, TestProbe, ImplicitSender, TestKit}
import org.scalatest.matchers.MustMatchers
import org.scalatest.{Ignore, BeforeAndAfterAll, WordSpecLike}
import pl.edu.agh.scala.auctionhouse.AuctionHouseMessage.{BidOnAuction, MakeBidOnInterestingAuction, RelistAuction, PutOnAuction}
import pl.edu.agh.scala.auctionhouse.AuctionNotification.AuctionRelistingPossible
import pl.edu.agh.scala.auctionhouse.AuctionSearchMessage.{FindAuctionReply, FindAuction, Register}

import scala.concurrent.duration.Duration

/**
 * Reactive Scala Programming 2014/2015. Exercise 2.
 *
 * Created by Magdalena Strzoda on 2014-11-02.
 */
@Ignore
class AuctionHouseSpec extends TestKit(ActorSystem())
  with WordSpecLike with BeforeAndAfterAll with ImplicitSender with MustMatchers {

  val data = AuctionHouseTestData

  override def afterAll() = system.shutdown() //TestKit.shutdownActorSystem(system)

  "An AuctionHouse" must {

    // seller test

    "have a seller" which {

      "creates an auction" in {
        val seller = system.actorOf(Props(classOf[Seller], 0, data.auctionNames(0)))
        seller ! PutOnAuction(0)
      }

      "relists their auction when possible" in {
        val seller = system.actorOf(Props(classOf[Seller], 0, data.auctionNames(0)))
        seller ! AuctionRelistingPossible

        expectMsg(RelistAuction)
      }

    }

    // buyer test

    "have a buyer" which {

      // TODO: fixme
      "have buyers who bid on auctions after getting response from AuctionSearch" ignore {
        val auctionSearch = system.actorOf(Props(classOf[MasterSearch], data.searchActorsNumber))
        val auction = TestProbe()
        auctionSearch ! Register(auction.ref, data.auctionNames(0)(1))
        val buyer = TestActorRef(new Buyer(0, data.buyerMaxBidAmount(0)))
        val keyword = "manuscript"

        buyer ! MakeBidOnInterestingAuction(keyword)

        auction.expectMsgType[BidOnAuction]

        Thread.sleep(4000)
      }

      "have buyers who query AuctionSearch" ignore {
        val auctionSearch = TestProbe()
        //      val auctionSearch = TestActorRef(new AuctionSearch())
        val buyer = TestActorRef(new Buyer(0, data.buyerMaxBidAmount(0)))
        val keyword = "diesel"

        buyer ! MakeBidOnInterestingAuction(keyword)

        auctionSearch.expectMsg(FindAuction(keyword))
      }

    }

    // auction search test

    "make finding auction in AuctionSearch possible" in {
      val auctionSearch = system.actorOf(Props(classOf[MasterSearch], data.searchActorsNumber))
      val seller = TestProbe()
      val auction = TestActorRef(new Auction(0, seller.ref, data.auctionNames(0)(0)))
      auctionSearch ! Register(auction, data.auctionNames(0)(0))

      auctionSearch ! FindAuction("diesel")

      expectMsg(List[ActorRef](auction))
      auction ! PoisonPill
    }


    // whole system test

   "run the whole house till auction ends" ignore {

      val sellers = new Array[ActorRef](data.sellersNumber)
      val buyers = new Array[ActorRef](data.buyersNumber)

      val masterSearch = TestActorRef(new MasterSearch(data.searchActorsNumber))

      var i = 0
      while ( i < data.sellersNumber ) {
        sellers(i) = TestActorRef(new Seller(i, data.auctionNames(i)), "seller_" + i)
        var j = 0
        while ( j < data.auctionNames(i).length ) {
          sellers(i) ! PutOnAuction(j)
          j += 1
        }
        i += 1
      }

      Thread.sleep(2000)  // let all the registrations take place

      i = 0
      while ( i < data.buyersNumber ) {
        buyers(i) = TestActorRef(new Buyer(i, data.buyerMaxBidAmount(i)), "buyer_" + i)
        i += 1
      }

      i = 0
      while ( i < data.buyersNumber ) {
        //  val buyerId = rand.nextInt(buyers.size)
        for ( keyword <- data.buyerKeywords(i) ) {
          buyers(i) ! MakeBidOnInterestingAuction(keyword)

        }
        Thread.sleep(1000)
        i+= 1
      }

    }

  }

}
