package pl.edu.agh.scala.auctionhouse

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.testkit._
import org.scalatest._
import org.scalatest.matchers.MustMatchers
import pl.edu.agh.scala.auctionhouse.AuctionHouseMessage._
import pl.edu.agh.scala.auctionhouse.AuctionNotification.{AuctionRelistingPossible, NotifyBidder, NotifyWinner, RegisterForNotifications}

import scala.concurrent.duration.FiniteDuration

/**
 * Reactive Scala Programming 2014/2015. Exercise 2.
 *
 * Created by Magdalena Strzoda on 2014-10-29.
 */
//
//abstract class FSMSpec extends TestKit(ActorSystem()) with ImplicitSender
//  with WordSpecLike with MustMatchers with BeforeAndAfterAll {
//
//  override def afterAll() = system.shutdown() //TestKit.shutdownActorSystem(system)
//
//}
//
//class AuctionSpec extends FSMSpec {
//
//  "An Auction" must {
//    "start in AuctionCreated state" in {
//      val seller = TestProbe()
//      val auction = TestFSMRef(new Auction(0, seller.ref,  "description"))
//
//      auction ! CreateAuction
//
//      auction.stateName must be(AuctionCreated)
//    }
//
//    "receive an initial bid and change its state to Activated" in {
//      val seller = TestProbe()
//      val auction = TestFSMRef(new Auction(0, seller.ref,  "description"))
//
//      // when
//      auction ! CreateAuction
//      auction ! BidOnAuction(self, 10)
//
//      // then
//      auction.stateName must be(AuctionActivated)
//      auction.stateData must be(AuctionData(self, 10))
//    }
//
//    "change its state to ignored if timer expires" in {
//      val seller = TestProbe()
//      val auction = TestFSMRef(new Auction(0, seller.ref,  "description"))
//
//      // when
//      auction ! CreateAuction
//      auction ! IgnoreAuction
//
//      // then
//      auction.stateName must be(AuctionIgnored)
//    }
//
//    "give chance for someone to bid before delete timer expires" in {
//      val seller = TestProbe()
//      val buyer = TestProbe()
//      val auction = TestFSMRef(new Auction(0, seller.ref,  "description"))
//
//      // when
//      auction ! CreateAuction
//      auction ! IgnoreAuction
//      auction.stateName must be(AuctionIgnored)
//
//      // then
//      seller.expectMsg(AuctionRelistingPossible)
//    }
//
//    "change its state when relisted" in {
//      val seller = TestProbe()
//      val buyer = TestProbe()
//      val auction = TestFSMRef(new Auction(0, seller.ref,  "description"))
//      auction.setState(AuctionIgnored, AuctionData(null, 0))
//
//      // when
//      auction ! RelistAuction
//
//      // then
//      auction.stateName must be(AuctionCreated)
//    }
//
//    "be removed after second timer expires and it wasn't relisted" in {
//      val seller = TestProbe()
//      val auction = TestFSMRef(new Auction(0, seller.ref,  "description"))
//      auction.setState(AuctionIgnored, AuctionData(seller.ref, 0))
//
//      // when
//      auction ! DeleteAuction
//
//      auction.isTerminated must be(right = true)
//    }
//
//    "become sold only after one bid" in {
//      val seller = TestProbe()
//      val buyer = TestProbe()
//      val auction = TestFSMRef(new Auction(0, seller.ref,  "description"))
//      auction.setState(AuctionActivated, AuctionData(buyer.ref, 20))
//
//      auction.stateName must be(AuctionActivated)
//      auction ! IgnoreAuction
//
//      auction.stateName must be(AuctionSold)
//    }
//
//    "change its internal state and data after bids" in {
//      val seller = TestProbe()
//      val buyer1 = TestProbe()
//      val buyer2 = TestProbe()
//      val auction = TestFSMRef(new Auction(0, seller.ref,  "description"))
//      auction.setState(AuctionActivated, AuctionData(buyer1.ref, 20))
//
//      // when
//      auction.stateName must be(AuctionActivated)
//      auction ! BidOnAuction(buyer2.ref, 30)
//
//      // then
//      auction ! IgnoreAuction
//      auction.stateName must be(AuctionSold)
//      auction.stateData must be(AuctionData(buyer2.ref, 30))
//    }
//
//    "ignore invalid bids" in {
//      val seller = TestProbe()
//      val buyer1 = TestProbe()
//      val buyer2 = TestProbe()
//      val auction = TestFSMRef(new Auction(0, seller.ref,  "description"))
//      auction.setState(AuctionActivated, AuctionData(buyer1.ref, 20))
//
//      auction ! BidOnAuction(buyer2.ref, 10)
//
//      auction.stateData must be(AuctionData(buyer1.ref, 20))
//    }
//
//
//    "notify winners when auction ends" in {
//      val buyer1 = TestProbe()
//      val seller = TestProbe()
//      val auction = TestFSMRef(new Auction(0, seller.ref,  "description"))
//      auction.setState(AuctionSold, AuctionData(buyer1.ref, 20))
//
//      auction ! DeleteAuction
//
//      buyer1.expectMsg(FiniteDuration.apply(10,  TimeUnit.SECONDS), NotifyWinner(0))
//    }
//
//    "notify buyers on rebidding" in {
//      val seller = TestProbe()
//      val buyer1 = TestProbe()
//      val buyer2 = TestProbe()
//      val buyer3 = TestProbe()
//      val auction = TestFSMRef(new Auction(0, seller.ref,  "description"))
//      auction.setState(AuctionCreated, AuctionData(null,0))
//      buyer1.send(auction, RegisterForNotifications(buyer1.ref))
//      buyer2.send(auction, RegisterForNotifications(buyer2.ref))
//      buyer3.send(auction, RegisterForNotifications(buyer3.ref))
//      auction.setState(AuctionActivated, AuctionData(buyer1.ref, 10))
//
//      // when
//      buyer2.send(auction, BidOnAuction(buyer2.ref, 20))
//
//      buyer1.expectMsg(NotifyBidder(20, auction))
//      buyer3.expectMsg(NotifyBidder(20, auction))
//    }
//
//    "be destroyed after selling" in {
//      val seller = TestProbe()
//      val buyer1 = TestProbe()
//      val auction = TestFSMRef(new Auction(0, seller.ref, "description"))
//      auction.setState(AuctionSold, AuctionData(buyer1.ref, 20))
//
//      auction ! DeleteAuction
//
//      auction.isTerminated must be(right = true)
//    }
//
//  }
//
//}
